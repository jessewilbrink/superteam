package nl.jessewilbrink.schooltool.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.jessewilbrink.schooltool.entity.User;
import nl.jessewilbrink.schooltool.service.UserService;

@RestController
public class UserController {
	@Autowired
	@Qualifier("userService")
	private UserService userService;

	@PostMapping("/user/maak/{gebruikersnaam}/{wachtwoord}/{emailadres}/{voornaam}/{tussenvoegsel}/{achternaam}/{adres}/{plaats}/{postcode}/{overmij}")
	public void createUser(@PathVariable(value = "gebruikersnaam") String gebruikersnaam,
			@PathVariable(value = "wachtwoord") String wachtwoord,
			@PathVariable(value = "emailadres") String emailadres, @PathVariable(value = "voornaam") String voornaam,
			@PathVariable(value = "tussenvoegsel", required = false) String tussenvoegsel,
			@PathVariable(value = "achternaam") String achternaam, @PathVariable(value = "adres") String adres,
			@PathVariable(value = "plaats") String plaats, @PathVariable(value = "postcode") String postcode,
			@PathVariable(value = "overmij") String overmij) {
		tussenvoegsel = tussenvoegsel.trim();
		overmij = overmij.trim();
		User user = new User(gebruikersnaam, wachtwoord, emailadres, voornaam, tussenvoegsel, achternaam, adres, plaats,
				postcode, overmij);
		userService.addUser(user);
	}

	@PutMapping("/user/update")
	public void changeExistingUser(@Valid @RequestBody User user) {
		userService.modifyUser(user);
	}

	@DeleteMapping("/user/verwijder/{id}")
	public String removeUser(@PathVariable(value = "id") Long id) {
		String message;
		if (userService.getUserOpId(id) != null) {
			userService.removeUser(id);
			message = "User is verwijderd";
		} else {
			message = "User bestaat niet en kan niet verwijderd worden";
		}
		return message;
	}

	@GetMapping("/user/get/{id}")
	public User viewUserById(@PathVariable(value = "id") Long id) {
		return userService.getUserOpId(id);
	}

	@GetMapping("/user/getalle")
	public List<User> viewAllUsereren() {
		return userService.getAllUsers();
	}
}
