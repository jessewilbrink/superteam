package nl.jessewilbrink.schooltool.dao;

import java.util.List;

public interface DaoInterface<T> {
	T get(Long id);

	void add(T entity);

	void update(T entity);

	void delete(Long id);

	List<T> getAll();

}
