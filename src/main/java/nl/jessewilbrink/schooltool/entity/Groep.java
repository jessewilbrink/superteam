package nl.jessewilbrink.schooltool.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Groep {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String naam;
	@OneToMany(mappedBy = "groep", cascade = CascadeType.ALL)
	@Column(nullable = true)
	private Set<Kind> kind;
	@ManyToMany
	private Set<Leerkracht> leerkracht;

	public Groep(String naam, Set<Kind> kind, Set<Leerkracht> leerkracht) {
		setNaam(naam);
		setKind(kind);
		setLeerkracht(leerkracht);
	}

	public Groep(String naam) {
		setNaam(naam);
	}

	public Groep() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public Set<Kind> getKind() {
		return kind;
	}

	public void setKind(Set<Kind> kind) {
		this.kind = kind;
	}

	public Set<Leerkracht> getLeerkracht() {
		return leerkracht;
	}

	public void setLeerkracht(Set<Leerkracht> leerkracht) {
		this.leerkracht = leerkracht;
	}
}
