package nl.jessewilbrink.schooltool.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.jessewilbrink.schooltool.entity.Leerkracht;
import nl.jessewilbrink.schooltool.service.LeerkrachtService;

@RestController
public class LeerkrachtController {
	@Autowired
	@Qualifier("leerkrachtService")
	private LeerkrachtService leerkrachtService;

	@PostMapping("/leerkracht/maak/{voornaam}/{achternaam}")
	public void createLeerkracht(@PathVariable(value = "voornaam") String voornaam,
			@PathVariable(value = "achternaam") String achternaam) {
		Leerkracht leerkracht = new Leerkracht(voornaam, achternaam);
		leerkrachtService.addLeerkracht(leerkracht);
	}

	@PutMapping("/leerkracht/update")
	public void changeExistingLeerkracht(@Valid @RequestBody Leerkracht leerkracht) {
		leerkrachtService.modifyLeerkracht(leerkracht);
	}

	@DeleteMapping("/leerkracht/verwijder/{id}")
	public String removeLeerkracht(@PathVariable(value = "id") Long id) {
		String message;
		if (leerkrachtService.getLeerkrachtOpId(id) != null) {
			leerkrachtService.removeLeerkracht(id);
			message = "Leerkracht verwijderd";
		} else {
			message = "Leerkracht bestaat niet en kan niet verwijderd worden";
		}
		return message;
	}

	@GetMapping("/leerkracht/get/{id}")
	public Leerkracht viewLeerkrachtById(@PathVariable(value = "id") Long id) {
		return leerkrachtService.getLeerkrachtOpId(id);
	}

	@GetMapping("/leerkracht/getalle")
	public List<Leerkracht> viewAllKinderen() {
		return leerkrachtService.getAllLeerkrachten();
	}
}
