package nl.jessewilbrink.schooltool.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.jessewilbrink.schooltool.entity.Kind;
import nl.jessewilbrink.schooltool.service.KindService;

@RestController
public class KindController {
	@Autowired
	@Qualifier("kindService")
	private KindService kindService;

	@PostMapping("/kind/maak/{voornaam}/{achternaam}")
	public void createKind(@PathVariable(value = "voornaam") String voornaam,
			@PathVariable(value = "achternaam") String achternaam) {
		Kind kind = new Kind(voornaam, achternaam);
		kindService.addKind(kind);
	}

	@PutMapping("/kind/update")
	public void changeExistingKind(@Valid @RequestBody Kind kind) {
		kindService.modifyKind(kind);
	}

	@DeleteMapping("/kind/verwijder/{id}")
	public String removeKind(@PathVariable(value = "id") Long id) {
		String message;
		if (kindService.getKindOpId(id) != null) {
			kindService.removeKind(id);
			message = "Kind is verwijderd";
		} else {
			message = "Kind bestaat niet en kan niet verwijderd worden";
		}
		return message;
	}

	@GetMapping("/kind/get/{id}")
	public Kind viewKindById(@PathVariable(value = "id") Long id) {
		return kindService.getKindOpId(id);
	}

	@GetMapping("/kind/getalle")
	public List<Kind> viewAllKinderen() {
		return kindService.getAllKinderen();
	}
}
