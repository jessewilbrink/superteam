package nl.jessewilbrink.schooltool.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Leerkracht {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String voornaam;
	private String achternaam;
	@ManyToMany
	@Column(nullable = true)
	private Set<Groep> groep;

	public Leerkracht(String voornaam, String achternaam, Set<Groep> groep) {
		setVoornaam(voornaam);
		setAchternaam(achternaam);
		setGroep(groep);
	}

	public Leerkracht(String voornaam, String achternaam) {
		setVoornaam(voornaam);
		setAchternaam(achternaam);
	}

	public Leerkracht() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public Set<Groep> getGroep() {
		return groep;
	}

	public void setGroep(Set<Groep> groep) {
		this.groep = groep;
	}
}
