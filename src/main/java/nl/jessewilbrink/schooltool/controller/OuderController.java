package nl.jessewilbrink.schooltool.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.jessewilbrink.schooltool.entity.Ouder;
import nl.jessewilbrink.schooltool.service.OuderService;

@RestController
public class OuderController {
	@Autowired
	@Qualifier("ouderService")
	private OuderService ouderService;

	@PostMapping("/ouder/maak/{voornaam}/{achternaam}")
	public void createOuder(@PathVariable(value = "voornaam") String voornaam,
			@PathVariable(value = "achternaam") String achternaam) {
		Ouder ouder = new Ouder(voornaam, achternaam);
		ouderService.addOuder(ouder);
	}

	@PutMapping("/ouder/update")
	public void changeExistingOuder(@Valid @RequestBody Ouder ouder) {
		ouderService.modifyOuder(ouder);
	}

	@DeleteMapping("/ouder/verwijder/{id}")
	public String removeOuder(@PathVariable(value = "id") Long id) {
		String message;
		if (ouderService.getOuderOpId(id) != null) {
			ouderService.removeOuder(id);
			message = "Ouder verwijderd";
		} else {
			message = "Ouder bestaat niet en kan niet verwijderd worden";
		}
		return message;
	}

	@GetMapping("/ouder/get/{id}")
	public Ouder viewOuderById(@PathVariable(value = "id") Long id) {
		return ouderService.getOuderOpId(id);
	}

	@GetMapping("/ouder/getalle")
	public List<Ouder> viewAllKinderen() {
		return ouderService.getAllOuderen();
	}
}
