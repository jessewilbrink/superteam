package nl.jessewilbrink.schooltool.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.jessewilbrink.schooltool.entity.Groep;
import nl.jessewilbrink.schooltool.service.GroepService;

@RestController
public class GroepController {
	@Autowired
	@Qualifier("groepService")
	private GroepService groepService;

	@PostMapping("/groep/maak/{naam}")
	public void createGroep(@PathVariable(value = "naam") String naam) {
		Groep groep = new Groep(naam);
		groepService.addGroep(groep);
	}

	@PutMapping("/groep/update")
	public void changeExistingGroep(@Valid @RequestBody Groep groep) {
		groepService.modifyGroep(groep);
	}

	@DeleteMapping("/groep/verwijder/{id}")
	public String removeGroep(@PathVariable(value = "id") Long id) {
		String message;
		if (groepService.getGroepOpId(id) != null) {
			groepService.removeGroep(id);
			message = "Groep verwijderd";
		} else {
			message = "Groep bestaat niet en kan niet verwijderd worden";
		}
		return message;
	}

	@GetMapping("/groep/get/{id}")
	public Groep viewGroepById(@PathVariable(value = "id") Long id) {
		return groepService.getGroepOpId(id);
	}

	@GetMapping("/groep/getalle")
	public List<Groep> viewAllKinderen() {
		return groepService.getAllGroepen();
	}
}
