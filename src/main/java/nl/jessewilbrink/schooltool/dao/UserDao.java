package nl.jessewilbrink.schooltool.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import nl.jessewilbrink.schooltool.entity.User;

@Repository("userDao")
public class UserDao implements DaoInterface<User> {

	@PersistenceContext
	private EntityManager em;

	@Override
	public User get(Long id) {
		return em.find(User.class, id);
	}

	@Override
	public void add(User entity) {
		em.persist(entity);
	}

	@Override
	public void update(User entity) {

		User upTeDatenUser = get(entity.getId());

		upTeDatenUser.setGebruikersnaam(entity.getGebruikersnaam());
		upTeDatenUser.setWachtwoord(entity.getWachtwoord());
		upTeDatenUser.setEmailadres(entity.getEmailadres());
		upTeDatenUser.setVoornaam(entity.getVoornaam());
		upTeDatenUser.setTussenvoegsel(entity.getTussenvoegsel());
		upTeDatenUser.setAchternaam(entity.getAchternaam());
		upTeDatenUser.setAdres(entity.getAdres());
		upTeDatenUser.setPlaats(entity.getPlaats());
		upTeDatenUser.setPostcode(entity.getPostcode());
		upTeDatenUser.setOvermij(entity.getOvermij());

		em.flush();
	}

	@Override
	public void delete(Long id) {
		em.remove(get(id));
	}

	@Override
	public List<User> getAll() {
		Query query = em.createQuery("from User as usr order by usr.id");
		return query.getResultList();
	}
}
