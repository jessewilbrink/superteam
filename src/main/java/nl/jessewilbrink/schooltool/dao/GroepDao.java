package nl.jessewilbrink.schooltool.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import nl.jessewilbrink.schooltool.entity.Groep;

@Repository("groepDao")
public class GroepDao implements DaoInterface<Groep> {
	@PersistenceContext
	private EntityManager em;

	@Override
	public Groep get(Long id) {
		return em.find(Groep.class, id);
	}

	@Override
	public void add(Groep entity) {
		em.persist(entity);
	}

	@Override
	public void update(Groep entity) {
		Groep upTeDatenGroep = get(entity.getId());

		upTeDatenGroep.setNaam(entity.getNaam());

		em.flush();
	}

	@Override
	public void delete(Long id) {
		em.remove(get(id));
	}

	@Override
	public List<Groep> getAll() {
		Query query = em.createQuery("from Groep as gr order by gr.id");
		return query.getResultList();
	}
}
