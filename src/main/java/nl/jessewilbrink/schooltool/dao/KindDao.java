package nl.jessewilbrink.schooltool.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import nl.jessewilbrink.schooltool.entity.Kind;

@Repository("kindDao")
public class KindDao implements DaoInterface<Kind> {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Kind get(Long id) {
		return em.find(Kind.class, id);
	}

	@Override
	public void add(Kind entity) {
		em.persist(entity);
	}

	@Override
	public void update(Kind entity) {

		Kind upTeDatenKind = get(entity.getId());

		upTeDatenKind.setVoornaam(entity.getVoornaam());
		upTeDatenKind.setAchternaam(entity.getAchternaam());

		em.flush();
	}

	@Override
	public void delete(Long id) {
		em.remove(get(id));
	}

	@Override
	public List<Kind> getAll() {
		Query query = em.createQuery("from Kind as knd order by knd.id");
		return query.getResultList();
	}
}
