package nl.jessewilbrink.schooltool.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.jessewilbrink.schooltool.dao.GroepDao;
import nl.jessewilbrink.schooltool.entity.Groep;

@Service("groepService")
public class GroepService {
	@Autowired
	@Qualifier("groepDao")
	private GroepDao groepDao;

	@Transactional
	public Groep getGroepOpId(Long id) {
		return groepDao.get(id);
	}

	@Transactional
	public void addGroep(Groep groep) {
		groepDao.add(groep);
	}

	@Transactional
	public void modifyGroep(Groep groep) {
		groepDao.update(groep);
	}

	@Transactional
	public List<Groep> getAllGroepen() {
		return groepDao.getAll();

	}

	@Transactional
	public void removeGroep(Long id) {
		groepDao.delete(id);

	}
}
