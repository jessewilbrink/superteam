package nl.jessewilbrink.schooltool.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import nl.jessewilbrink.schooltool.entity.Leerkracht;

@Repository("leerkrachtDao")
public class LeerkrachtDao implements DaoInterface<Leerkracht> {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Leerkracht get(Long id) {
		return em.find(Leerkracht.class, id);
	}

	@Override
	public void add(Leerkracht entity) {
		em.persist(entity);

	}

	@Override
	public void update(Leerkracht entity) {
		Leerkracht upTeDatenLeerkracht = get(entity.getId());

		upTeDatenLeerkracht.setVoornaam(entity.getVoornaam());
		upTeDatenLeerkracht.setAchternaam(entity.getAchternaam());

		em.flush();
	}

	@Override
	public void delete(Long id) {
		em.remove(get(id));
	}

	@Override
	public List<Leerkracht> getAll() {
		Query query = em.createQuery("from Leerkracht as lkc order by lkc.id");
		return query.getResultList();
	}

}
