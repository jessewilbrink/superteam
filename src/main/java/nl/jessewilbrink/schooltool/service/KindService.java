package nl.jessewilbrink.schooltool.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.jessewilbrink.schooltool.dao.KindDao;
import nl.jessewilbrink.schooltool.entity.Kind;

@Service("kindService")
public class KindService {
	@Autowired
	@Qualifier("kindDao")
	private KindDao kindDao;

	@Transactional
	public Kind getKindOpId(Long id) {

		return kindDao.get(id);
	}

	@Transactional
	public void addKind(Kind kind) {
		kindDao.add(kind);
	}

	@Transactional
	public void modifyKind(Kind kind) {
		kindDao.update(kind);
	}

	@Transactional
	public List<Kind> getAllKinderen() {
		return kindDao.getAll();

	}

	@Transactional
	public void removeKind(Long id) {
		kindDao.delete(id);

	}
}
