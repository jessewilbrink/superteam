package nl.jessewilbrink.schooltool.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import nl.jessewilbrink.schooltool.entity.Ouder;

@Repository("ouderDao")
public class OuderDao implements DaoInterface<Ouder> {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Ouder get(Long id) {
		return em.find(Ouder.class, id);
	}

	@Override
	public void add(Ouder entity) {
		em.persist(entity);
	}

	@Override
	public void update(Ouder entity) {
		Ouder upTeDatenOuder = get(entity.getId());

		upTeDatenOuder.setVoornaam(entity.getVoornaam());
		upTeDatenOuder.setAchternaam(entity.getAchternaam());

		em.flush();
	}

	@Override
	public void delete(Long id) {
		em.remove(get(id));
	}

	@Override
	public List<Ouder> getAll() {
		Query query = em.createQuery("from Ouder as od order by od.id");
		return query.getResultList();
	}

}
