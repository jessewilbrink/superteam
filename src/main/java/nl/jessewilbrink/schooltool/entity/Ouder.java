package nl.jessewilbrink.schooltool.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Ouder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String voornaam;
	private String achternaam;
	@ManyToMany
	@Column(nullable = true)
	private Set<Kind> kind;

	public Ouder(String voornaam, String achternaam, Set<Kind> kind) {
		setVoornaam(voornaam);
		setAchternaam(achternaam);
		setKind(kind);
	}

	public Ouder(String voornaam, String achternaam) {
		setVoornaam(voornaam);
		setAchternaam(achternaam);
	}

	public Ouder() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public Set<Kind> getKind() {
		return kind;
	}

	public void setKind(Set<Kind> kind) {
		this.kind = kind;
	}
}
