package nl.jessewilbrink.schooltool.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.jessewilbrink.schooltool.dao.LeerkrachtDao;
import nl.jessewilbrink.schooltool.entity.Leerkracht;

@Service("leerkrachtService")
public class LeerkrachtService {
	@Autowired
	@Qualifier("leerkrachtDao")
	private LeerkrachtDao leerkrachtDao;

	@Transactional
	public Leerkracht getLeerkrachtOpId(Long id) {
		return leerkrachtDao.get(id);
	}

	@Transactional
	public void addLeerkracht(Leerkracht leerkracht) {
		leerkrachtDao.add(leerkracht);
	}

	@Transactional
	public void modifyLeerkracht(Leerkracht leerkracht) {
		leerkrachtDao.update(leerkracht);
	}

	@Transactional
	public List<Leerkracht> getAllLeerkrachten() {
		return leerkrachtDao.getAll();

	}

	@Transactional
	public void removeLeerkracht(Long id) {
		leerkrachtDao.delete(id);

	}
}
