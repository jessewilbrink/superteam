package nl.jessewilbrink.schooltool.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.jessewilbrink.schooltool.dao.UserDao;
import nl.jessewilbrink.schooltool.entity.User;

@Service("userService")
public class UserService {
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;

	@Transactional
	public User getUserOpId(Long id) {

		return userDao.get(id);
	}

	@Transactional
	public void addUser(User user) {
		userDao.add(user);
	}

	@Transactional
	public void modifyUser(User user) {
		userDao.update(user);
	}

	@Transactional
	public List<User> getAllUsers() {
		return userDao.getAll();

	}

	@Transactional
	public void removeUser(Long id) {
		userDao.delete(id);

	}
}
