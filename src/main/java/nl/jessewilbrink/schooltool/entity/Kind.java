package nl.jessewilbrink.schooltool.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Kind {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String voornaam;
	private String achternaam;
	@ManyToMany
	@Column(nullable = true)
	private Set<Ouder> ouder;
	@ManyToOne(optional = true)
	private Groep groep;

	public Kind(String voornaam, String achternaam, Set<Ouder> ouder, Groep groep) {
		super();
		setVoornaam(voornaam);
		setAchternaam(achternaam);
		setOuder(ouder);
		setGroep(groep);
	}

	public Kind(String voornaam, String achternaam, Set<Ouder> ouder) {
		super();
		setVoornaam(voornaam);
		setAchternaam(achternaam);
		setOuder(ouder);
	}

	public Kind(String voornaam, String achternaam, Groep groep) {
		super();
		setVoornaam(voornaam);
		setAchternaam(achternaam);
		setGroep(groep);
	}

	public Kind(String voornaam, String achternaam) {
		super();
		setVoornaam(voornaam);
		setAchternaam(achternaam);
	}

	public Kind() {

	}

	public Groep getGroep() {
		return groep;
	}

	public void setGroep(Groep groep) {
		this.groep = groep;
	}

	public Set<Ouder> getOuder() {
		return ouder;
	}

	public void setOuder(Set<Ouder> ouder) {
		this.ouder = ouder;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}
}
