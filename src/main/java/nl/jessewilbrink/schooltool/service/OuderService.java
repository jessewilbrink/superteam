package nl.jessewilbrink.schooltool.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.jessewilbrink.schooltool.dao.OuderDao;
import nl.jessewilbrink.schooltool.entity.Ouder;

@Service("ouderService")
public class OuderService {
	@Autowired
	@Qualifier("ouderDao")
	private OuderDao ouderDao;

	@Transactional
	public Ouder getOuderOpId(Long id) {
		return ouderDao.get(id);
	}

	@Transactional
	public void addOuder(Ouder ouder) {
		ouderDao.add(ouder);
	}

	@Transactional
	public void modifyOuder(Ouder ouder) {
		ouderDao.update(ouder);
	}

	@Transactional
	public List<Ouder> getAllOuderen() {
		return ouderDao.getAll();

	}

	@Transactional
	public void removeOuder(Long id) {
		ouderDao.delete(id);

	}
}
