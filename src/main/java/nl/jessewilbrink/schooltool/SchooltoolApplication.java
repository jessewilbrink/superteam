package nl.jessewilbrink.schooltool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchooltoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchooltoolApplication.class, args);
	}

}
