package nl.jessewilbrink.schooltool.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, unique = true)
	private String gebruikersnaam;
	private String wachtwoord;
	private String emailadres;
	private String voornaam;
	private String tussenvoegsel;
	private String achternaam;
	private String adres;
	private String plaats;
	private String postcode;
	private String overmij;

	public User(String username, String wachtwoord) {
		setGebruikersnaam(username);
		setWachtwoord(wachtwoord);
	}

	public User() {
	}

	public User(String gebruikersnaam, String wachtwoord, String emailadres, String voornaam, String tussenvoegsel,
			String achternaam, String adres, String plaats, String postcode, String overmij) {
		setGebruikersnaam(gebruikersnaam);
		setWachtwoord(wachtwoord);
		setEmailadres(emailadres);
		setVoornaam(voornaam);
		setTussenvoegsel(tussenvoegsel);
		setAchternaam(achternaam);
		setAdres(adres);
		setPlaats(plaats);
		setPostcode(postcode);
		setOvermij(overmij);
	}

	public String getGebruikersnaam() {
		return gebruikersnaam;
	}

	public void setGebruikersnaam(String gebruikersnaam) {
		this.gebruikersnaam = gebruikersnaam;
	}

	public String getWachtwoord() {
		return wachtwoord;
	}

	public void setWachtwoord(String wachtwoord) {
		this.wachtwoord = wachtwoord;
	}

	public Long getId() {
		return id;
	}

	public String getEmailadres() {
		return emailadres;
	}

	public void setEmailadres(String emailadres) {
		this.emailadres = emailadres;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getTussenvoegsel() {
		return tussenvoegsel;
	}

	public void setTussenvoegsel(String tussenvoegsel) {
		this.tussenvoegsel = tussenvoegsel;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getPlaats() {
		return plaats;
	}

	public void setPlaats(String plaats) {
		this.plaats = plaats;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getOvermij() {
		return overmij;
	}

	public void setOvermij(String overmij) {
		this.overmij = overmij;
	}

}
